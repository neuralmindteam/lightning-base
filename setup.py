
from setuptools import find_packages, setup

setup(
    name='lightningbase',
    packages=find_packages(),
    version='0.1.1',
    description='Base Class For Pytorch Lightning',
    author='NeuralMind',
    license='MIT',
    install_requires=[
        "pytorch-lightning>=0.7.6<=0.8.4",
    ],
)
