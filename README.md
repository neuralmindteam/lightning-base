# Lightning Base

## Instalação

1.  Clone o repositório

    - HTTPS: `git clone https://<username>@bitbucket.org/neuralmindteam/ligthning-base.git`
    - SSH: `git clone git@bitbucket.org:neuralmindteam/lightning-base.git`

1.  Entre no diretório

    - `cd lightning-base`

1.  Instale no seu ambiente
    - `pip install -e .`

Após a instalação poderá fazer o **import** como um pacote no _Python_

```python
from lightningbase import LightningBase
```

## Uso

- Cheque o notebook **MNIST.ipynb** para exemplo de uso.
- Cheque a documentacação do [Pytorch Lightning](https://pytorch-lightning.readthedocs.io/en/stable/lightning-module.html#) para mais opções de uso
