import numpy as np
import matplotlib.pyplot as plt
# steps: number of thresholds used the estimate the score distributions


def far_vs_frr(probs, labels, title, steps=1000):
    '''
        Arguments:
          - probs (np.array): probabilities, array with values in [0,1]
          - labels (np.array): array with values in {0, 1}
          - title (str): title of the plot
          - steps (int): number of steps (thresholds) that will be tested
        Outputs:
          - ERR (float): Minimum error
          - Threshold (float): threshold that generates the minimum error
    '''

    lives = probs[labels == 1]
    spoofs = probs[labels == 0]
    max_live = max(lives)
    min_spoof = min(spoofs)
    num_lives = len(lives)
    num_spoofs = len(spoofs)
    print('Lives: ', num_lives, 'Spoofs: ', num_spoofs)
    histogram = np.linspace(0, 1, num=steps)
    FAR = np.array([100*np.mean(spoofs >= hist) for hist in histogram])
    FRR = np.array([100*np.mean(lives < hist) for hist in histogram])
    a = FRR[FRR - FAR <= 0]   
    s_a = len(a)
    if (FAR[s_a] - FRR[s_a]) <= (FRR[s_a+1]-FAR[s_a+1]):
        ERR = (FAR[s_a] + FRR[s_a])/2
        ERR_index = s_a
    else:
        ERR = (FAR[s_a+1] + FRR[s_a+1])/2
        ERR_index = s_a+1
    figure = plt.figure(num=1, figsize=(8, 6), dpi=80,
                        facecolor='w', edgecolor='k')
    ax = figure.add_subplot(111)
    plt.plot(histogram, FRR, color='r', label='FRR')
    plt.plot(histogram, FAR, color='b', label='FAR')
    plt.plot([histogram[ERR_index]], [ERR], marker='o',
             markersize=6, color="green", label='ERR: %0.2f' % (ERR))
    axes = plt.gca()
    axes.set_ylim([0, 3*ERR])
    plt.title('FAR vs FRR graph '+title)
    plt.legend()
    plt.show()
    figure.savefig('./'+'far_vs_frr_'+title+'.png')
    return ERR, histogram[ERR_index]
