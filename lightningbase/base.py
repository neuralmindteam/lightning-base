from typing import Dict, List

import torch
from torch.utils.data import DataLoader, Dataset


class LightningBase:

    '''
        hparams needs to contain:
            - lr (float)
            - optimizer (str)
            - optimizer_kwargs (Dict[str,Optional])
            - train_batch_size (int)
            - val_batch_size (int)
            - shuffle_train (bool)
            - num_workers (int)
        Properties needed:
            - train_dataset (Dataset)
            - val_dataset (Dataset)
            - test_dataset (Dataset)
    '''

    def _average_key(self, outputs: List[Dict[str, torch.Tensor]], key: str) -> torch.FloatTensor:
        return torch.stack([o[key] for o in outputs]).float().mean()

    def _concat_lists_by_key(self, outputs, key):
        return sum([o[key] for o in outputs], [])

    def get_dataloader(self, dataset: Dataset, batch_size: int, shuffle: bool, num_workers: int) -> DataLoader:
        return DataLoader(
            dataset,
            batch_size=batch_size,
            shuffle=shuffle,
            num_workers=num_workers
        )

    def get_optimizer(self,) -> torch.optim.Optimizer:
        optimizer_name = self.hparams.optimizer
        lr = self.hparams.lr
        optimizer_hparams = self.hparams.optimizer_kwargs
        optimizer = getattr(torch.optim, optimizer_name)
        return optimizer(self.parameters(), lr=lr, **optimizer_hparams)

    ## AS FUNÇÕES ABAIXO SÃO NECESSÁRIAS PARA O PYTORCH LIGHTNING ##

    def train_dataloader(self,) -> DataLoader:
        return self.get_dataloader(
            self.train_dataset,
            batch_size=self.hparams.train_batch_size,
            shuffle=self.hparams.shuffle_train,
            num_workers=self.hparams.num_workers
        )

    def val_dataloader(self,) -> DataLoader:
        return self.get_dataloader(
            self.valid_dataset,
            batch_size=self.hparams.val_batch_size,
            shuffle=False,
            num_workers=self.hparams.num_workers
        )

    def test_dataloader(self,) -> DataLoader:
        return self.get_dataloader(
            self.test_dataset,
            batch_size=self.hparams.val_batch_size,
            shuffle=False,
            num_workers=self.hparams.num_workers
        )

    def configure_optimizers(self) -> torch.optim.Optimizer:
        optimizer = self.get_optimizer()
        return optimizer
